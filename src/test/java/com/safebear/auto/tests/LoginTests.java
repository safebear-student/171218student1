package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test

    public void loginTest(){

        // Step 1 Action Go To Login URL
        driver.get(Utils.getURL());

        // Step 1 EXPECTED RESULT: check we're on the Login Page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The Login Page didn't open, or the title text has changed");

        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 EXPECTED RESULT: check that we're now on the Tools Page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "The Login Page didn't open, or the title text has changed");

        // Step 4 EXPECTED RESULT: check for Login Successful message on Tools Page
        Assert.assertTrue(toolsPage.getloginSuccessMessage().contains("Success"));

    }

    @Test

    public void failedLogin(){
        // Step 1 Action Go To Login URL
        driver.get(Utils.getURL());

        // Step 1 EXPECTED RESULT: check we're on the Login Page
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The Login Page didn't open, or the title text has changed");

        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("Evelyn");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();

        // Step 4 EXPECTED: User cannot log in
        Assert.assertEquals(loginPage.warningLoginMessage(),"WARNING: Username or Password is incorrect");
    }



}
